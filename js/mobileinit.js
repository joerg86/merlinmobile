$(document).bind("mobileinit", function(){
  $.mobile.loadingMessage = "Laden...";
  $.mobile.pageLoadErrorMessage = "Fehler beim Laden der Seite";
  $.mobile.page.prototype.options.backBtnText = "Zurück";
  $.mobile.defaultPageTransition = "none";
});