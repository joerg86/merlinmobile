if(typeof Merlin == "undefined")
	var Merlin = {};

// Konstanten
Merlin.SUCHE_XML = "http://fileas.nibis.de/harry/potter/suche.xml";

// Parst die <data>-Tags im XML und gibt eine Liste mit JavaScript-Objekten zurück
$.extend(Merlin, {
	backend: {
		// Initialisiert den Speicher für Downloads
		initStorage: function() {
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, Merlin.backend._storageReady, Merlin.backend._storageFail);
		},
		// Dateisystem ist bereit, Download-Ordner erstellen oder holen
		_storageReady: function(fs) {
			fs.root.getDirectory("MerlinMobile", {	create: true}, function(dir) {
				Merlin.backend.downloadDir = dir;
			});
		},
		// Liste der aktuellen Downloads
		downloads: [],
		// Download hinzufügen
		addDownload: function(item, url, callback) {
			var down = {
				item: item,
				status: "PENDING",
				url: item.media_url,
				filename: item.parent_identifier + "-" + item.path.split("/").pop()
			}
			
			// Media-URL überschreiben (wichtig für geschütze Downloads)
			if(url)
				down.url = url;
			
			down.path = Merlin.backend.downloadDir.fullPath + "/" + down.filename;
			Merlin.backend.downloads.push(down);
			Merlin.backend._startDownload(down, callback);
		},
		// Interne Funktion, die einen Download in Gang setzt
		_startDownload: function(down, callback) {
			var ft = new window.plugins.phoneLoad.FileTransfer();
			ft.download(down.url, down.path, 
				function(res) {
					// Download erfolgreich
				    if(res.completed)
				    {
				    	down.status = "DONE";
				    	down.item.local_url = "file://" + down.path;
				    	Merlin.backend.saveBookmarkFolder();
				    }
				    else
				    {
				    	down.status = "RUNNING";
				    }
			    	down.percent = res.percentCompleted;
			    	down.received = res.bytesReceived;
			    	down.total = res.bytesTotal;
					if(callback)
						callback(down, ft);
				},
				function() {
					// Download abgebrochen
					down.status = "ERROR";
					if(callback)
						callback(down, ft);	
				}

			);
		},
		// Startet die NiBis-Authentifizierung
		startAuth: function(username, password, identifier, callback) {
			var url = "http://test.search.merlin.nibis.de/auth.php?cmd=true&identifier="+ encodeURIComponent(identifier) + "&json=1";
			console.log("AUTH URL "+url);
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				cache: false,
				data: {username: username, password: password},
				success: function(data, status, xhr) {
					var authurl = data.redirecturl;
					callback(authurl);
				},
				error: function(xhr, status, e) {
					callback(null);
				}
			});
		},

		// Speichert übergordnete Ordner zwischen: parentCache[ordner] = parent
		_folders: [],
		_parentCache: {},
		getParent: function(folder) {
			var folders = Merlin.backend._folders;
			var idx = $.inArray(folder, folders);
			console.log("getParent: idx=" + idx);
			if(idx == -1)
				return null
				else
				{	
					var pidx = Merlin.backend._parentCache[idx];
					if(pidx != null)
						return folders[pidx]
					else
						return null;
				}
		},
		setParent: function(folder, parent) {
			var folders = Merlin.backend._folders;
			var fidx = $.inArray(folder, folders);
			var pidx = $.inArray(parent, folders);
			
			if(pidx == -1)
			{
				folders.push(parent);
				pidx = folders.length - 1;
			}
			if(fidx == -1)
			{
				folders.push(folder);
				fidx = folders.length - 1;
			}
			Merlin.backend._parentCache[fidx] = pidx;
		},
		// Liefert eine Liste der Bookmarks
		getBookmarkFolder: function() {
			// JSON nur parsen, wenn die Bookmarkliste noch nicht im Cache ist.
			if(!Merlin.backend._bookmarkFolder)
				Merlin.backend._bookmarkFolder = $.parseJSON(window.localStorage.getItem("bookmarkFolder"));
			var folder = Merlin.backend._bookmarkFolder;
			
			if(folder != null)
				return folder;
				else return { name: "Meine Medien", folders: [], bookmarks: [] };	
		},
		// Speichert die Bookmarks
		saveBookmarkFolder: function() {
			var folder = Merlin.backend.getBookmarkFolder();
			var json = JSON.stringify(folder);
			window.localStorage.setItem("bookmarkFolder", json);
			Merlin.backend._bookmarkFolder = folder;
		},
		// Gibt eine Liste mit Merlin/Elixier-Objekten zurück
		getXMLItems: function(data)
		{
			var result = [];
			$(data).find("data").each(function(idx, item){
				item = $(item);
				var o = {};
				o.identifier = item.find("identifier").text();
				o.parent_identifier = item.find("parent_identifier").text();
				o.path = item.find("path").text();
				o.title = item.find("title").text();
				o.description = item.find("description").text();
				o.logo = item.find("logo").text();
				o.thumbnail = item.find("thumbnail").text();
				o.media_url = item.find("media_url").text();
				o.item_count = parseInt(item.find("item_count").text());

				o.is_merlin = false;
				if (item.find("is_merlin").text() == "1")
					o.is_merlin = true;

				if(!o.item_count)
					o.item_count = 0;

				// Typ-Bezeichner der Ressource setzen
				o.type = "Elixier-Ressource";
				if(o.is_merlin && !o.parent_identifier)
					o.type = "Merlin-Hauptmedium"
						else if (o.is_merlin)
							o.type = "Merlin-Einzelobjekt";

				// Sicherstellen, dass wir nur absolute URLs haben
				if(o.media_url && !o.media_url.match(/^http:/))
					o.media_url = "http://search.merlin.nibis.de/" + o.media_url;
				if(o.thumbnail && !o.thumbnail.match(/^http:/))
					o.thumbnail = "http://search.merlin.nibis.de/" + o.thumbnail;
				if(o.logo && !o.logo.match(/^http:/))
					o.logo = "http://search.merlin.nibis.de/" + o.logo;

				result.push(o);
			});
			return result;
		},
		// Facetten aus dem XML-Dokument extrahieren
		getFacets: function(data)
		{
			var facets = {};
			$(data).find("field").each(function() {
				var name = $(this).attr("name");
				var tokens = [];
				$(this).find("token").each(function() {
					var t = {};
					t.count = parseInt($(this).attr("count"));
					t.value = $(this).attr("value");
					tokens.push(t);
				});
				facets[name] = tokens;
			});
			return facets;
		},
		// Suchergebnisse aus XML-Daten laden und in den DOM der Seite packen
		loadXMLResults: function(data, querystring)
		{
			var page = $("#search-results-" + hex_md5(querystring));
			if(!page.length)
			{
				// jQuery-Objekt der Seite holen
				page = $("#search-results-template").clone(false);
				page.attr("id", "search-results-"+ hex_md5(querystring));
				$("body").append(page);

				// Infos zur Pagination des Eregebnisses laden
				var sum = parseInt($(data).find("sum").text());
				if(sum == 0)
					return false; // nichts gefunden
				var start = parseInt($(data).find("start").text());
				var end = parseInt($(data).find("sumAnzahl").text());
				var count = parseInt($(data).find("anzahl").text());

				// Formulardaten extrahieren (Stichwort)
				var d = querystring2dict(querystring);
				var stichwort = d.stichwort;

				if(!stichwort && d.mediennummer)
					stichwort = d.mediennummer;

				// Facetten extrahieren
				var facets = Merlin.backend.getFacets(data);
				page.find("a.result-filter").jqmData("facets", facets);
				page.find("a.result-filter").jqmData("search-string", querystring);

				// Knöpfe zum Blättern ggf. aktivieren
				page.find("a.next-page").removeClass("ui-btn-active").addClass("ui-disabled").removeAttr("data-search-string");
				page.find("a.prev-page").removeClass("ui-btn-active").addClass("ui-disabled").removeAttr("data-search-string");

				if(end < sum)
				{
					d.start = end;
					page.find("a.next-page").removeClass("ui-disabled").jqmData("search-string", dict2querystring(d));
				}
				if(start > 0)
				{
					d.start = Math.max(start - count, 0);
					page.find("a.prev-page").removeClass("ui-disabled").jqmData("search-string", dict2querystring(d));
				}

				// Infos über das Suchergebnis in die Seite einfügen
				page.find(".search-start").text(start+1); // Eins addieren, da interne Seitennummern bei 0 beginnen
				page.find(".search-end").text(end);
				page.find(".search-sum").text(sum);
				page.find(".search-term").text(stichwort);

				// Infos zum Filter einblenden
				if(d["lernressourcentyp[]"] || d.bildungsebene || d.fachsach)
				{
					var f = [];
					if(d["lernressourcentyp[]"])
						f.push(d["lernressourcentyp[]"]);
					if(d.fachsach)
						f.push(d.fachsach);
					if(d.bildungsebene)
						f.push(d.bildungsebene);
					page.find(".filter-info").text("Filter: " + f.join("; "));
				}

				// Suchergebnisliste leeren
				var ul = page.find(".resultlist");

				// Für jedes <data>-Tag ein <li>-Objekt erzeugen
				var items = Merlin.backend.getXMLItems($(data).find("items"));
				$.each(items, function(idx, o){
					var elem = Merlin.views.getListItem(o); // <li>-Tag erzeugen lassen

					// Hauptmedium hervorheben, wenn nach Einzelobjekten gesucht wurde
					if(d.mediennummer && (o.identifier == d.mediennummer))
						elem.jqmData("theme", "e");
					
					elem.appendTo(ul); // An Suchergebnisliste anhängen
				});
			}
			else {
				page.find(".ui-btn-active").removeClass("ui-btn-active");
			}
			return "#"+page.attr("id");
		},
		// Legt ein Medium in einem Ordner als Bookmark ab
		addBookmark: function(item, folder) {
			if(!folder)
				folder = Merlin.backend.getBookmarkFolder();
			
			// Liste mit IDs der Bookmarks in diesem Ordner
			var ids = $.map(folder.bookmarks, function(i) { return i.identifier});
			
			if($.inArray(item.identifier, ids) != -1)
				// Existiert schon in diesem Ordner
				return false
				else
				{
					// Hinzufügen
					item.is_bookmark = true;
					folder.bookmarks.push(item);
					Merlin.backend.saveBookmarkFolder();
					return true;
				}
			
		}
	},
	views: {
		// Erzeugt ein <li>-Element aus einem übergebenen Merlin/Elixier-Objekt
		getListItem: function(o) {
			var imgdata;
			if(o.thumbnail)
				imgdata = "<img src='" + o.thumbnail + "'> ";
			else
				imgdata = "<img src='" + o.logo + "'> ";



			var elem = $("<li><a href='#' class='show-item'>"+imgdata+"<h5>"+o.title+"</h5><p><em>"+o.type+"</em></p></a></li>");
			elem.find("a").jqmData("object", o); 


			return elem;
		},
		// Zeigt ein einzelnes Merlin-Objekt an
		showItem: function(item)
		{
			var page = $("#item-info-"+hex_md5(item.identifier));

			if(!page.length)
			{
				page = $("#item-info").clone(false);
				page.attr("id", "item-info-"+hex_md5(item.identifier));
				page.find(".title").text(item.title);
				page.find(".type").text(item.type);
				page.find(".description").html(item.description);
				page.find("a.bookmark").jqmData("item", item);
				
				if(item.is_merlin && item.parent_identifier)
					page.find(".description").append("<p><em>Dieses Einzelobjekt gehört zum Medium Nr. "+item.parent_identifier+"</em></p>")
				
				console.log("LOCAL URL " + item.local_url);
			    if (item.local_url)
			    	page.find(".media_url").attr("href", item.local_url)
			    else
			    	page.find(".media_url").attr("href", item.media_url);
				
				// Wenn das Objekt aus dem Merlin-Pool stammt und nicht heruntergeladen ist,
		        // dann muss eine Authentifizierung bei NiBis stattfinden.
				if((item.is_merlin) && (!item.local_url))
				{
					page.find(".media_url").addClass("auth");
					page.find(".media_url").attr("href", item.identifier);
				}
					
				// Button für Einzelobjektsuche anzeigen, wenn Einzelobjekte vorhanden
				if(item.item_count > 0)
				{
					page.find(".item_count").text(item.item_count);
					page.find(".show-subitems").jqmData("search-string", "explicit=1&mediennummer=" + encodeURIComponent(item.identifier));
				}
				else
					page.find(".show-subitems").remove();

				if(item.parent_identifier)
				{
					page.find(".parent_identifier").text(item.parent_identifier);
					page.find(".show-main").jqmData("search-string", "explicit=1&mediennummer=" + encodeURIComponent(item.parent_identifier));
					page.find(".show-main").show();
				}
				else
					page.find(".show-main").remove();
				
				// Download-Button einblenden bei Merlin-Einzelobjekten
				if((item.is_merlin) && (item.parent_identifier) && (item.is_bookmark))
					page.find(".add-download").removeClass("ui-disabled");
				else
					page.find(".add-download").addClass("ui-disabled");
				
				// Status-Kügelchen updaten
				Merlin.views.updateStatusBullet(item, page);
				
				page.find(".add-download").jqmData("item", item);
				

				var imgurl;
				if(item.thumbnail)
					imgurl = item.thumbnail
					else
						imgurl = item.logo;	

				page.find(".thumb").attr("src", imgurl);
				page.appendTo($("body"));
			}
			else {
				// Seite etwas aufräumen
				page.find(".ui-btn-active").removeClass("ui-btn-active");
			}
			$.mobile.changePage("#"+page.attr("id"));
		},
		// Aktualisiert die Status-Anzeige (rot, gelb, grün)
		updateStatusBullet: function(item, page) {
			if (page == null) page = $("#item-info-"+hex_md5(item.identifier));
			var sbul = page.find("img.status-bullet");
			if((item.is_merlin) && (item.parent_identifier))
			{
				if(item.local_url)
					sbul.attr("src", "img/bullet_green.png");
				else
					sbul.attr("src", "img/bullet_yellow.png");
			}
			else
			{
					sbul.attr("src", "img/bullet_red.png");
			}
		},
		// Startet eine Merlin-Suchanfrage
		search: function(querystring, direction)
		{
			var url = Merlin.SUCHE_XML;
			$.mobile.showPageLoadingMsg();

			// AJAX-Request an die XML-Schnittstelle bei NiBis
			$.post(url, querystring, function(data) {
				var resultpage = Merlin.backend.loadXMLResults(data, querystring);

				if (resultpage)
					if(direction == "reverse")
						$.mobile.changePage(resultpage, { reverse: true });
					else
						$.mobile.changePage(resultpage);
				else
					$.mobile.changePage("#no-results", { transition: "pop"});
			});	
		},
		// Facetten: Links für einzelne Token in eine <ul> kippen
		appendTokensToList: function(ul, tokens, querystring, fieldname)
		{
			var d = querystring2dict(querystring);
			d.start = "0";
			$.each(tokens.slice(0,6), function() {
				var elem = $("<li><a href='#' class='search'>" + this.value + '<span class="ui-li-count">' + this.count + "<span></a></li>");
				d[fieldname] = this.value;
				elem.find("a").jqmData("search-string", dict2querystring(d));
				ul.append(elem);
			});
		},
		// Zeigt den Ordner "Meine Medien" an.
		showMyMedia: function(folder, parent, noChange) {
			var page = $("#mymedia");
			if(folder == null)
				folder = Merlin.backend.getBookmarkFolder();
			
			if((folder != Merlin.backend.getBookmarkFolder()) && !parent)
			{
				parent = Merlin.backend.getParent(folder);
			}
			
			var ul = page.find("ul.resultlist");
			ul.empty();
			
			if(folder.folders.length)
			{
				// Listentrenner "Ordner"
				ul.append($("<li data-role='list-divider'>Ordner</li>"));

				$.each(folder.folders, function() {
					var count = this.folders.length + this.bookmarks.length;
					var elem = $("<li><a href='#' class='show-mymedia'>"+this.name+"<span class='ui-li-count'>" + count + "</span></a>"
							+ "<a href=# class='delete-folder'>Löschen</a></li>");
					elem.find(".show-mymedia").jqmData("folder", this);
					elem.find(".show-mymedia").jqmData("parent", folder);
					elem.find(".delete-folder").jqmData("parent", folder);
					elem.find(".delete-folder").jqmData("folder", this);
					
					// Elternordner im Cache merken
					Merlin.backend.setParent(this, folder);
					ul.append(elem);
				});
			}
			
			ul.append($("<li data-role='list-divider'>Medien</li>"));
			
			$.each(folder.bookmarks, function() {
				var item = this;
				var li = Merlin.views.getListItem(item);
				
				// den Löschen-Link hinzufügen
				var dellink = $("<a href='#' class='delete-bookmark'>Löschen</a>");
				dellink.jqmData("item", item);
				dellink.jqmData("folder", folder);
				li.append(dellink);
				
				// An die Liste anhängen
				ul.append(li);
			});
			
			// Meldung anzeigen, falls der Ordner leer ist.
			if(folder.bookmarks.length == 0)
				page.find(".no-objects").show()
				else
					page.find(".no-objects").hide();
			
			// Zahl der Objekte und Namen des Ordners einfügen
			page.find(".media-count").text(folder.bookmarks.length + folder.folders.length);
			page.find(".folder-name").text(folder.name);
			
			// Funktionen zum Verwalten von Ordnern
			page.find("a.add-folder").jqmData("parent", folder);
			page.find("a.rename-button").jqmData("parent", parent);
			page.find("a.rename-button").jqmData("folder", folder);
			
			if(!parent)
				{
					page.find(".up-button").hide()
					page.find(".rename-button").hide();
				}
				else 
				{
					page.find(".rename-button").show();
					page.find(".up-button").show();
					page.find(".up-button").jqmData("folder", parent);
				}

			
			// Seite parsen / ListView aktualisieren
			page.page();
			ul.listview();
			ul.listview("refresh");
			page.trigger("create");
			
			// Parameter merken (für Refresh)
			page.jqmData("folder", folder);
			page.jqmData("parent", parent);
			
			if(!noChange)
				$.mobile.changePage("#mymedia");
		},
		// Aktualisiert den zuletzt angezeigten Bookmark-Ordner
		// (Damit beim Drücken der Zurück-Taste kein veraltetes Ergebnis gezeigt wird)
		refreshMyMedia: function() {
			var page= $("#mymedia");
			var folder = page.jqmData("folder");
			var parent = page.jqmData("parent");
			Merlin.views.showMyMedia(folder, parent, true);
		},
		// Zeigt den Ordner-Auswahldialog
		showFolderChoice: function(callback, folder, parent) {
			if(!folder)
				folder = Merlin.backend.getBookmarkFolder();
			if(!parent)
				parent = Merlin.backend.getParent(folder);
			
			var page = $("#folder-choice");
			page.find(".folder-name").text(folder.name);
			page.find(".add-folder").jqmData("parent", folder);
			page.find(".up-button").jqmData("folder", parent);
			page.find(".up-button").jqmData("callback", callback);
			if(parent)
				page.find(".up-button").removeClass("ui-disabled")
				else
					page.find(".up-button").addClass("ui-disabled");
			
			var viewFunc = function(f, p) { Merlin.views.showFolderChoice(callback, f, p)};
			page.find(".add-folder").jqmData("viewFunc", viewFunc);
			
			var ul = page.find(".folder-list");
			ul.empty();
			
			var cur = $("<li data-theme='e'><a href='#'>"+ folder.name + "</a><a href='#' class='choose'>Wählen</a></li>");
			// Callback aufrufen, wenn der Ordner gewählt wurde
			cur.find(".choose").click(function(e){
				$("#manage-folder.ui-dialog,#folder-choice.ui-dialog").dialog("close");
				callback(folder);
			});
			
			ul.append($("<li data-role='list-divider'>Aktueller Ordner</li>"));
			ul.append(cur);
			if(folder.folders.length)
				ul.append($("<li data-role='list-divider'>Unterordner von \""+folder.name+"	\"</li>"));
			
			$.each(folder.folders, function() {
				var subfolder = this;
				var elem = $(
					"<li><a href='#' class='browse'>" + this.name + "</a><a href='#' class='choose'>Wählen</a>" +
					+"</li>"
				);
				// Parent in den Cache bringen
				Merlin.backend.setParent(subfolder, folder);
				
				// Unterordner anzeigen
				elem.find(".browse").click(function(e) {
					Merlin.views.showFolderChoice(callback, subfolder);
				});
				// Callback aufrufen, wenn ein Ordner gewählt wurde
				elem.find(".choose").click(function(e){
					var manage = $("#manage-folder.ui-dialog");
					
					// Callback aufrufen, wenn der Dialog geschlossen wird
					$("#folder-choice").one("pagehide", function() {
						callback(subfolder);
					})
					
					// Wenn ein "Ordner hinzufügen"-Dialog noch geöffnet ist, diesen vorher schließen
					if(manage.length)
					{
						manage.one("pagehide", function() {
							$("#folder-choice").dialog("close");
						});
						manage.dialog("close");
					}
					else
						$("#folder-choice").dialog("close");
					
				});
				ul.append(elem);
			});
			page.page();
			ul.listview();
			ul.listview("refresh");
			if($.mobile.activePage != "#folder-choice")
				$.mobile.changePage("#folder-choice");
		},
		doAuth: function(identifier, callback) {
			var authCB = function(authurl) {
				var form = $(".auth-form");
				var url = $(".auth-form").attr("action");
				if(authurl)
				{
					callback(authurl);
				}
				else {
					$.mobile.changePage("#auth-dialog");
					form.find(".message").show(300).delay(1000).hide(300);
				}
			}
			$(".auth-form").attr("action", identifier);
			$(".auth-form").jqmData("callback", authCB);
			var username = $("#username").val();
			var password = $("#password").val();
			if(username)
			{
				Merlin.backend.startAuth(username, password, identifier, authCB);
			}
			else $.mobile.changePage("#auth-dialog");
		},
		// Aktualisiert die Seite mit den Downloads
		refreshDownload: function(down, filetrans) {
			var page = $("#download-manager");
			var item_page = $("#item-info-"+hex_md5(down.item.identifier));
			var ul = page.find("#download-list");
			
			var elem;
			var created = false;
			
			ul.find("li").each(function() {
				if($(this).jqmData("download").path == down.path)
					elem = $(this);
			});
			if(!elem)
			{
				elem = $("<li><a class='show-item' href='#'><h3></h3><p></p></a><a class='cancel-download' href='#'>Löschen</a></li>");
				elem.find("a").jqmData("object", down.item);
				elem.find("a").jqmData("filetransfer", filetrans);
				created = true;
			}
			// STatustext erstellen
			var statustext;
			if(down.status == "PENDING")
				statustext = "Warten...";
			else if (down.status == "DONE")
			{
				statustext = "Fertig";
				// User benachrichtigen
				navigator.notification.vibrate(1000);
				navigator.notification.beep(1);
				jqmSimpleMessage("Ein Download wurde abgeschlossen.");
				
				// lokale URL in die Einezlinfo-Seite einfügen
				item_page.find(".media_url").attr("href", "file://" + down.path);
				item_page.find(".media_url").removeClass("auth");
				Merlin.views.updateStatusBullet(down.item);
				
				// Download-Button wieder aktivieren (wenn jemand das nochmal herunterladen will)
				item_page.find(".add-download").removeClass("ui-disabled");
				item_page.find(".add-download .ui-btn-text").text("Download");

			}
			else if (down.status == "RUNNING")
			{
				statustext = "Herunterladen... (" + down.percent.toFixed(1) +"%)";
				item_page.find(".add-download").addClass("ui-disabled");
				item_page.find(".add-download .ui-btn-text").text(down.percent.toFixed(1) + "%");
			}
			else
			{
				statustext = "Fehlgeschlagen.";
				item_page.find(".add-download").removeClass("ui-disabled");
				item_page.find(".add-download .ui-btn-text").text("Download");
			}
			
			elem.jqmData("download", down);
			elem.find("h3").text(down.filename);
			elem.find("p").text(statustext);
			
			if(created)
			{
				ul.prepend(elem);
				page.page();
				ul.listview("refresh");
			}
			
			item_page.find(".add-download").trigger("create");
		}
	}
});
$(document).ready(function() {
	document.addEventListener("deviceready", function() {
		// Download-Storage initialisieren
		Merlin.backend.initStorage();
		// Suche starten, wenn das Suchformular abgesendet wird
		$(".search_form").submit(function(e) {
			var querystring = $(this).serialize();
			Merlin.views.search(querystring);
			return false;
		});
		// Toolbars zeigen beim Druck der Menütaste
		/*
		$(document).bind("menubutton", function(e) {
			$.mobile.fixedToolbars.show();
		});
		*/
		// Zurück-Taste
		$(document).bind("backbutton", function(e) {
			history.back();
			return false;
		});
		
		// Listview updaten, wenn ein neues Ergebnis/neue Facetten gezeigt wird.
		$("#result-filter").bind("pagebeforeshow", function(e, data) {
			$(this).find("ul.facets").listview("refresh");
		})
		
		// Suche: Blättern (vor/zurück), Einzelobjekte
		$("a.search").live("click", function(e) {
			e.preventDefault();
			var direction = $(this).jqmData("direction");
			Merlin.views.search($(this).jqmData("search-string"), direction);
			return false;
		});
		
		// Merlin-Objekt anzeigen, wenn ein entsprechender Link geklickt wurde
		$("a.show-item").live("click", function(e) {
			var item = $(this).jqmData("object");
			Merlin.views.showItem(item);
		});
		$("a.media_url:not(.auth)").live("click", function(e) {
			e.preventDefault();
			openExternal($(this).attr("href"));
		});
		$("a.auth").live("click", function(e) {
			e.preventDefault();
			var identifier = $(this).attr("href");
			Merlin.views.doAuth(identifier, openExternal);
		});
		
		$(".auth-form").live("submit", function(e) {
			var username = $("#username").val();
			var password = $("#password").val();
			var callback = $(this).jqmData("callback");
			if(callback == null)
				callback = authcb;
			var identifier = $(this).attr("action");
			Merlin.backend.startAuth(username, password, identifier,function(url) {
				$("#auth-dialog").one("pagehide", function() {
					callback(url); 
				});
				$("#auth-dialog").dialog("close");
			});
			return false;
		});
		
		// Ordner "Meine Medien" anzeigen
		$("a.show-mymedia,#mymedia a.up-button").live("click", function(e) {
			var folder = $(this).jqmData("folder");
			var parent = $(this).jqmData("parent");
			
			Merlin.views.showMyMedia(folder, parent);
		});
		// Aufwärts-Knopf im Ordner-wählen-Dialog wurde gedrückt
		$("#folder-choice a.up-button").click(function(e) {
			$(this).removeClass("ui-btn-active");
			var folder = $(this).jqmData("folder");
			var callback = $(this).jqmData("callback");
			
			Merlin.views.showFolderChoice(callback, folder);	
		});
		// Einen neuen Ordner erstellen
		$("a.add-folder").live("click", function(e) {
			$(".folder-form").jqmData("parent", $(this).jqmData("parent"));
			$(".folder-form").jqmData("viewFunc", $(this).jqmData("viewFunc"));
			$(".folder-form").jqmData("folder", null);
			$("#folder-name").val("");
			$("#manage-folder .title").text("Neuer Ordner");
			$("#manage-folder .message").hide();
			$.mobile.changePage("#manage-folder");
		});
		// Einen Ordner umbenennen
		$("a.rename-button").live("click", function(e) {
			var folder = $(this).jqmData("folder");
			$(".folder-form").jqmData("parent", $(this).jqmData("parent"));
			$(".folder-form").jqmData("folder", folder);
			$("#manage-folder .title").text("Ordner umbenennen");
			$("#folder-name").val(folder.name);
			$("#manage-folder .message").hide();
			$.mobile.changePage("#manage-folder");	
		});
		// Einen Ordner löschen
		$("a.delete-folder").live("click", function(e) {
			var folder = $(this).jqmData("folder");
			var parent = $(this).jqmData("parent");
			var idx = $.inArray(folder, parent.folders);
			if(idx >= 0)
			{
				parent.folders.splice(idx, 1);
			}
			Merlin.backend.saveBookmarkFolder();
			$(this).parent("li").remove();
			$("#mymedia .resultlist").listview("refresh");
			var count = parseInt($("#mymedia .media-count").text());
			if(count)
				$("#mymedia .media-count").text(count - 1);
		});
		// Einen Download hinzufügen
		$("a.add-download").live("click", function(e) {
			var item = $(this).jqmData("item");
			Merlin.views.doAuth(item.identifier, function (url) {
				jqmSimpleMessage("Download wurde gestartet.");
				Merlin.backend.addDownload(item, url, Merlin.views.refreshDownload);
			});
		}); 

		// Einen laufenden Download abbrechen
		$("a.cancel-download").live("click", function(e) {
			e.preventDefault();
			console.log("DOWNLOAD stop requested.");
			var ft = $(this).jqmData("filetransfer");
			var link = this;	
			ft.cancel(function() {
				$(link).parent().remove();
				$("#download-list").listview("refresh");

			});
		});
		// Ein Objekt zu den Bookmarks hinzufügen
		$("a.bookmark").live("click", function(e) {
			var item = $(this).jqmData("item");
			var folder = Merlin.backend.getBookmarkFolder();

			// Ordner sind vorhanden -> Auswahldialog
			Merlin.views.showFolderChoice(function(folder) {
				if(Merlin.backend.addBookmark(item, folder))
				{
					jqmSimpleMessage("Bookmark in '"+folder.name+"' abgelegt.");
					Merlin.views.refreshMyMedia();
					var item_page = $("#item-info-"+hex_md5(item.identifier));
					
					// Downloadknopf ggf. freischalten
					if((item.is_merlin) && (item.parent_identifier))
						item_page.find(".add-download").removeClass("ui-disabled");
				}
				else
					jqmSimpleMessage("Bookmark existiert bereits.");
				//Merlin.views.showItem(item);
			});
		});
		
		// Ein Bookmark löschen
		$("a.delete-bookmark").live("click", function(e) {
			var item = $(this).jqmData("item");
			var folder = $(this).jqmData("folder");
			
			if(!folder)
				folder = Merlin.backend.getBookmarkFolder();
			var bookmarks = folder.bookmarks;
			var idx = $.inArray(item, bookmarks);
			if(idx >= 0)
			{
				bookmarks.splice(idx, 1);
				Merlin.backend.saveBookmarkFolder();
				$(this).parent("li").remove();
				$("#mymedia .resultlist").listview("refresh");

				var count = parseInt($("#mymedia .media-count").text());
				if(count)
					$("#mymedia .media-count").text(count - 1);
			}
		});
		
		// Einen Ordner erstellen oder umbenennen
		$(".folder-form").submit(function(e) {
			var folder = $(this).jqmData("folder");
			var parent = $(this).jqmData("parent");
			var viewFunc = $(this).jqmData("viewFunc");
			
			if(!parent)
				parent = Merlin.backend.getBookmarkFolder();
			
			if(!viewFunc)
				viewFunc = Merlin.views.showMyMedia;
			
			var name = $("#folder-name").val();
			
			var add = false;
			if(!folder)
			{
				add = true;
				folder = { name: name, folders: [], bookmarks: [] };
			} 
			
			var names = $.map(parent.folders, function(i) { return i.name });
			if($.inArray(name, names) != -1)
			{
				$("#manage-folder").find(".message").text("Ordner existiert schon. Bitte einen anderen Namen wählen.");
				$("#manage-folder").find(".message").show();
			}
			else
			{
				$("#manage-folder").find(".message").hide();
				if(add)
					parent.folders.push(folder);
				else
					folder.name = name;

				Merlin.backend.saveBookmarkFolder();
				$("#manage-folder").dialog("close");
				
				// Den Elternordner zeigen, wenn ein Ordner erstellt wurde, sonst den Ordner selbst
				if(add)
					viewFunc(parent);
				else
					viewFunc(folder, parent);
			}
			return false;
		});
		
		// Ergebnisfilter einblenden (Facetten)
		$("a.result-filter").live("click", function(e) {
			e.preventDefault();
			var page = $("#result-filter");
			var facets = $(this).jqmData("facets");
			var qs = $(this).jqmData("search-string");
			var ul = page.find("ul.facets");
			ul.empty();
			
			if(facets.lernressourcentyp)
			{
				ul.append($("<li data-role='list-divider'>Lernressourcentyp</li>"));
				Merlin.views.appendTokensToList(ul, facets.lernressourcentyp, qs, "lernressourcentyp[]");
			}
			if(facets.fachsach)
			{
				ul.append($("<li data-role='list-divider'>Fach-/Sachgebiet</li>"));
				Merlin.views.appendTokensToList(ul, facets.fachsach, qs, "fachsach");
			}
			if(facets.bildungsebene)
			{
				ul.append($("<li data-role='list-divider'>Bildungsebene</li>"));
				Merlin.views.appendTokensToList(ul, facets.bildungsebene, qs, "bildungsebene");
			}
			$.mobile.changePage("#result-filter");
		});
	});
});