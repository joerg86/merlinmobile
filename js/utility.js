function querystring2dict(q)
{
   var pairs = q.split("&");
   var d = {}
   for(var i=0; i<pairs.length;i++)
   {
      var params = pairs[i].split("=");
      if(params[0].length)
         d[decodeURIComponent(params[0])] = decodeURIComponent(params[1]);
   }
   return d;
}
function dict2querystring(d)
{
   var q = "";
   for(var key in d)
      q += "&" + encodeURIComponent(key) + "=" + encodeURIComponent(d[key]);
   return q;
}
function jqmSimpleMessage(message) {
    $("<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>" + message + "</h1></div>")
        .css({
            display: "block",
            opacity: 0.96,
            top: window.pageYOffset+140
        })
        .appendTo("body").delay(800)
        .fadeOut(400, function(){
            $(this).remove();
        });
}
function openExternal(url) {
	//window.plugins.childBrowser.showWebPage(url);

	window.plugins.webintent.startActivity({
	    action: WebIntent.ACTION_VIEW,
	    url: url
		},
	    function() {},
	    function() {
	    	jqmSimpleMessage("Objekt kann nicht angezeigt werden.");
	    }
	);
}

